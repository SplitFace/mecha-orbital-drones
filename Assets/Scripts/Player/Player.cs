﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour, IDamageable
{
    public PlayerWeapon rightWeapon;
    public PlayerWeapon leftWeapon;
    public PlayerWeapon shoulderWeapon;
    public PlayerWeapon auxiliaryWeapon;

    public float currentLife = 100f;
    public float maxLife = 100f;

    private bool isDead;
    public bool IsDead { get { isDead = currentLife > 0 ? false : true; return isDead; } }

    private void Awake()
    {
        PlayerWeapon[] allWeapons = new PlayerWeapon[4] { rightWeapon, leftWeapon, shoulderWeapon, auxiliaryWeapon };

        foreach (PlayerWeapon weapon in allWeapons)
        {
            if (weapon != null)
                weapon.playerOwner = this;
        }
    }
    public void OnDamaged(float damage)
    {
        if (!IsDead)
            currentLife -= damage;
    }

    void OnShoulderWeapon(InputValue value)
    {
        shoulderWeapon.ShootWeapon();
    }
}
