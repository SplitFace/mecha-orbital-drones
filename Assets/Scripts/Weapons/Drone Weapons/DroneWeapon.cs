﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(SphereCollider))]
public class DroneWeapon : PlayerWeapon
{
    public DroneBehaviour dronePrefab;

    public List<DroneBehaviour> drones = new List<DroneBehaviour>();
    public List<Transform> idlePositions = new List<Transform>();

    public delegate void triggerDelegate(Enemy enemy);
    public event triggerDelegate OnTriggerEvent;

    public float droneUptime = 10f;
    public float droneSearchRange = 25f;
    public float droneSpeed = 40f;
    public float droneAttackRange = 8f;

    public float droneMinDodgeTime = 0.5f;
    public float droneMaxDodgeTime = 1.5f;

    private DroneData droneData;

    private PoolingSystem<DroneBehaviour> poolingSystem = new PoolingSystem<DroneBehaviour>(
        x => x.isInactive == true,
        x => x.OrderBy(y => y.isInactive == true).ToList());

    void Start()
    {
        var collider = GetComponent<SphereCollider>();

        collider.isTrigger = true;
        collider.radius = droneSearchRange;

        droneData = new DroneData(this, playerOwner, projectilePrefab, droneUptime, droneSearchRange, droneSpeed, droneAttackRange, droneMinDodgeTime, droneMaxDodgeTime, shootDelay);

        if (drones.Count > 0)
        {
            poolingSystem.AddObject(drones.ToArray());
        }

        if (drones.Count < maxAmmo)
        {
            int dronesToSpawn = maxAmmo - drones.Count;

            poolingSystem.InitializePool(dronesToSpawn, dronePrefab, PoolInitializationCallback);
        }

        currentAmmo = maxAmmo;
    }

    public override void ShootWeapon()
    {
        if (currentAmmo > 0 && poolingSystem.CheckAvailableObject())
        {
            currentAmmo--;
            poolingSystem.NextObject.ActivateDrone();
        }
    }

    public void AttachDrone(DroneBehaviour drone)
    {
        drone.transform.position = transform.position;
        drone.transform.parent = transform;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Enemy")
        {
            OnTriggerEvent(other.GetComponent<Enemy>());
        }
    }

    void PoolInitializationCallback(List<DroneBehaviour> drones)
    {
        for (int i = 0; i < drones.Count; i++)
        {
            drones[i].droneData = droneData;

            drones[i].transform.position = transform.position;
            drones[i].transform.rotation = transform.rotation;
            drones[i].transform.parent = transform;

            drones[i].idlePosition = idlePositions[i];

            this.drones.Add(drones[i]);
        }
    }
}
