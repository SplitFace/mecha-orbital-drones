﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneBehaviour : MonoBehaviour
{
    public DroneWeapon weaponOwner = null;
    public Player playerOwner = null;
    public Transform idlePosition = null;
    public Projectile projectilePrefab = null;

    public DroneData droneData;

    public float uptime = 10f;
    public float searchRange = 25f;
    public float speed = 40f;
    public float attackRange = 8f;

    public float minDodgeTime = 0.5f;
    public float maxDodgeTime = 1.5f;

    public float shootDelay;
    public float shootTimer;

    public float projectileSpeed;
    public float damage;

    public bool isInactive = true;

    private Enemy currentEnemy;

    private Vector3 targetPosition;
    private Vector3 currentVelocity;

    private float uptimeTimer;
    private float attackPositionTimer;

    private bool enemyEngaged = false;

    public bool EnemyInRange
    {
        get
        {
            if (currentEnemy != null)
            {
                if (Vector3.Distance(currentEnemy.transform.position, targetPosition) <= attackRange)
                    return true;
                else
                    return false;
            }

            return false;
        }
    }

    private void Start()
    {
        Initialize();

        weaponOwner.OnTriggerEvent += EnemyDetected;
    }

    private void OnDestroy()
    {
        weaponOwner.OnTriggerEvent -= EnemyDetected;
    }

    public void Initialize()
    {
        weaponOwner = droneData.weaponOwner;
        playerOwner = droneData.playerOwner;
        projectilePrefab = droneData.projectilePrefab;
        uptime = droneData.uptime;
        searchRange = droneData.searchRange;
        speed = droneData.speed;
        attackRange = droneData.attackRange;
        minDodgeTime = droneData.minDodgeTime;
        maxDodgeTime = droneData.maxDodgeTime;
        shootDelay = droneData.shootDelay;
    }

    public void ActivateDrone()
    {
        if (isInactive)
        {
            transform.parent = null;
            isInactive = false;

            uptimeTimer = uptime;

            StartCoroutine(DroneBehaviours());
        }
    }

    public void EnemyDetected(Enemy enemy)
    {
        if (!enemyEngaged)
        {
            currentEnemy = enemy;
            enemyEngaged = true;
        }
    }

    private IEnumerator DroneBehaviours()
    {
        while (uptimeTimer > 0)
        {
            yield return new WaitForFixedUpdate();

            uptimeTimer -= Time.fixedDeltaTime;

            if (shootTimer > 0)
            {
                shootTimer -= Time.fixedDeltaTime;
            }
            else if (EnemyInRange)
            {
                ShootWeapon();
                shootTimer += shootDelay;
            }

            if (enemyEngaged && !currentEnemy.IsDead)
            {
                AttackBehaviour();
            }
            else
            {
                enemyEngaged = false;
                IdleBehaviour();
            }
        }

        attackPositionTimer = 0f;

        weaponOwner.AttachDrone(this);
        weaponOwner.currentAmmo++;

        isInactive = true;
        enemyEngaged = false;

        yield return null;
    }

    private void ShootWeapon()
    {

    }

    private void AttackBehaviour()
    {
        if (attackPositionTimer <= 0 || !EnemyInRange)
        {
            targetPosition = GetRandomPositon(currentEnemy.transform.position);
            attackPositionTimer = Random.Range(minDodgeTime, maxDodgeTime);
        }

        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref currentVelocity, 0.1f, speed);

        if (Vector3.Distance(transform.position, targetPosition) <= 0.3f)
        {
            attackPositionTimer -= Time.deltaTime;
        }
    }

    private void IdleBehaviour()
    {
        targetPosition = idlePosition.position;

        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref currentVelocity, 0.1f, speed);
    }

    private Vector3 GetRandomPositon(Vector3 position)
    {
        float randomX = Random.Range(position.x - (attackRange / 2f), position.x + (attackRange / 2f));
        float randomY = Random.Range(position.y, position.y + (attackRange / 2f));
        float randomZ = Random.Range(position.z - (attackRange / 2f), position.z + (attackRange / 2f));

        return new Vector3(randomX, randomY, randomZ);
    }
}

public struct DroneData
{
    public DroneData(DroneWeapon weaponOwner,
        Player playerOwner,
        Projectile projectilePrefab,
        float uptime,
        float searchRange,
        float speed,
        float attackRange,
        float minDodgeTime,
        float maxDodgeTime,
        float shootDelay)
    {
        this.weaponOwner = weaponOwner;
        this.playerOwner = playerOwner;
        this.projectilePrefab = projectilePrefab;
        this.uptime = uptime;
        this.searchRange = searchRange;
        this.speed = speed;
        this.attackRange = attackRange;
        this.minDodgeTime = minDodgeTime;
        this.maxDodgeTime = maxDodgeTime;
        this.shootDelay = shootDelay;
    }

    public DroneWeapon weaponOwner;
    public Player playerOwner;
    public Projectile projectilePrefab;

    public float uptime;
    public float searchRange;
    public float speed;
    public float attackRange;

    public float minDodgeTime;
    public float maxDodgeTime;

    public float shootDelay;
}
