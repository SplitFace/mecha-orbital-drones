﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;
    public Player player;

    public float speed = 12f;
    [Tooltip("This should be set at its default value: -9.81")]
    public float gravity = -9.81f;
    public float gravityMultiplier = 1.5f;
    public float jumpHeight = 3f;
    public int additiveJumps = 2;

    #region GROUND CHECK

    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    public Transform feet;

    bool isGrounded;

    #endregion

    int availableJumps;

    Vector2 movement;

    Vector3 playerVelocity;

    void Start()
    {
        if (controller == null)
            controller = GetComponent<CharacterController>();
        if (player == null)
            player = GetComponent<Player>();
    }

    void Update()
    {
        isGrounded = Physics.CheckSphere(feet.position, groundDistance, groundMask);

        if (isGrounded)
        {
            availableJumps = additiveJumps;
        }
    }

    void FixedUpdate()
    {
        #region MOVING PLAYER

        if (!player.IsDead)
        {
            Vector3 move = transform.right * movement.x + transform.forward * movement.y;

            controller.Move(move * speed * Time.fixedDeltaTime);
        }

        #endregion

        #region GRAVITY 

        if (!isGrounded)
        {
            playerVelocity.y += gravity * gravityMultiplier * Time.fixedDeltaTime;
        }
        else if (playerVelocity.y < 0)
        {
            playerVelocity.y = -2f;
        }

        controller.Move(playerVelocity * Time.fixedDeltaTime);

        #endregion
    }

    void OnMove(InputValue value)
    {
        movement = value.Get<Vector2>();
    }

    void OnJump(InputValue value)
    {
        if (availableJumps > 0 && !player.IsDead)
        {
            playerVelocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);

            availableJumps--;
        }
    }
}
