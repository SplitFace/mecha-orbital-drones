﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerWeapon : MonoBehaviour
{
    public enum WeaponType { rightArm, leftArm, shoulder, auxiliary }

    public WeaponType weaponType = WeaponType.rightArm;

    public Player playerOwner = null;
    public Projectile projectilePrefab;

    public int currentAmmo;
    public int maxAmmo;

    public float shootDelay;
    public float shootTimer;

    public abstract void ShootWeapon();
}
