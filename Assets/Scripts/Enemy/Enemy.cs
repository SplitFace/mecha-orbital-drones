﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour, IDamageable
{
    public float currentLife = 100f;
    public float maxLife = 100f;

    private bool isDead;
    public bool IsDead { get { isDead = currentLife > 0 ? false : true; return isDead; } }

    public void OnDamaged(float damage)
    {
        if (!IsDead)
            currentLife -= damage;

        if (IsDead)
            gameObject.SetActive(false);
    }
}
