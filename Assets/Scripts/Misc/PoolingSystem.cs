﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolingSystem<T> where T : Object
{
    private List<T> objectsPool = new List<T>();

    public delegate bool checkDelegate(T objectToCheck);
    private checkDelegate checkCondition;

    public delegate List<T> sortDelegate(List<T> list);
    private sortDelegate sortCondition;

    public delegate void callbackDelegate(List<T> item);

    private int currentIndex = 0;

    private bool objectChecked = false;

    public PoolingSystem(checkDelegate checkCondition)
    {
        this.checkCondition = checkCondition;
    }

    public PoolingSystem(checkDelegate checkCondition, sortDelegate sortCondition)
    {
        this.checkCondition = checkCondition;
        this.sortCondition = sortCondition;
    }

    public T NextObject
    {
        get
        {
            T nextObject = null;

            if (objectChecked)
            {
                nextObject = objectsPool[currentIndex++];

                if (currentIndex > (objectsPool.Count - 1))
                    currentIndex = 0;

                objectChecked = false;
            }

            return nextObject;
        }
    }

    public bool CheckAvailableObject()
    {
        if (checkCondition(objectsPool[currentIndex]))
        {
            objectChecked = true;
            return true;
        }
        else
        {
            sortCondition(objectsPool);
            currentIndex = 0;

            if (checkCondition(objectsPool[currentIndex]))
            {
                objectChecked = true;
                return true;
            }

            objectChecked = false;
            return false;
        }
    }

    public void InitializePool(int numberOfStartingItems, T item)
    {
        for (int i = 0; i < numberOfStartingItems; i++)
        {
            //Can also be changed to populate a pool outside of Unity
            T newObject = Object.Instantiate(item);
            objectsPool.Add(newObject);
        }
    }

    public void InitializePool(int numberOfStartingItems, T item, callbackDelegate callbackFunction)
    {
        List<T> newObjectsList = new List<T>();

        for (int i = 0; i < numberOfStartingItems; i++)
        {
            //Can also be changed to populate a pool outside of Unity
            T newObject = Object.Instantiate(item);

            objectsPool.Add(newObject);
            newObjectsList.Add(newObject);
        }

        callbackFunction(newObjectsList);
    }

    public void AddObject(T item)
    {
        objectsPool.Add(item);
    }

    public void AddObject(T[] items)
    {
        foreach (T item in items)
        {
            objectsPool.Add(item);
        }
    }
}
